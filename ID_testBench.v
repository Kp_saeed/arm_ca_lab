module ID_testBench();

reg flush = 0;
reg freeze = 0;
reg clk = 0;
reg rst = 0;
reg WB_en = 0; reg hazard = 0;
reg [31:0] instruction = 32'd4;
reg [31:0] result_wb = 32'd3;
reg [31:0] pc_in = 32'd1024;
reg [3:0] dest_wb = 4'b0100;
reg [3:0] sr = 4'b0000;
wire WB_en_out, Mem_R_en_out, Mem_W_en_out, B, S, imm, two_src,
ID_reg_WB_en, ID_reg_mem_R_en, ID_reg_mem_W_en, ID_reg_B, ID_reg_S, ID_reg_imm;
wire [31:0] instruction_out, val_rn, val_rm, pc,
ID_reg_pc, ID_reg_val_rn, ID_reg_val_rm, ID_reg_instruction;
wire [3:0] dest_out, src1, src2, EXE_CMD, ID_reg_EXE_CMD, ID_reg_dest,
ID_reg_src1, ID_reg_src2;
wire [11:0] shift_operand, ID_reg_shift_operand;
wire [23:0] Signed_imm, ID_reg_Signed_imm;

initial begin
    #19 rst = 1;
    #13 rst = 0;
end

always begin
clk = #50 !clk;
end

ID_Stage mem_stage(clk, rst, instruction, result_wb, WB_en, dest_wb,
            hazard, sr, pc_in, WB_en_out, Mem_R_en_out, Mem_W_en_out,
            B, S, EXE_CMD, val_rn, val_rm, instruction_out, imm,
            shift_operand, Signed_imm, dest_out, src1, src2, two_src, pc);
            
ID_Stage_Reg mem_stage_reg(clk, rst, flush, WB_en_out, Mem_R_en_out,
                Mem_W_en_out, B, S, EXE_CMD, pc, val_rn, val_rm, imm,
                shift_operand, Signed_imm, dest_out, instruction_out,
                src1, src2, ID_reg_WB_en, ID_reg_mem_R_en, ID_reg_mem_W_en,
                ID_reg_B, ID_reg_S, ID_reg_EXE_CMD, ID_reg_pc, ID_reg_val_rn,
                ID_reg_val_rm, ID_reg_imm, ID_reg_shift_operand,
                ID_reg_Signed_imm, ID_reg_dest, ID_reg_instruction,
                ID_reg_src1, ID_reg_src2);

endmodule





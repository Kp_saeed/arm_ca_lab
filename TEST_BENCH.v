module TEST_BENCH();

reg clk = 0;
reg rst = 0;

initial begin
    #19 rst = 1;
    #13 rst = 0;
end

always begin
clk = #10 !clk;
end

TOP_ARM arm(clk,rst);

endmodule

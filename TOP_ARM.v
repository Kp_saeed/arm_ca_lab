module TOP_ARM (
	input clk, rst
);

wire flush, freeze;
wire branch_taken;

wire[31:0] if_pc_stage, if_instruction_stage, branch_addr, if_pc_stage_reg, if_instruction_stage_reg;
wire[31:0] id_pc_stage, id_pc_stage_reg;
wire[31:0] exe_pc_stage, exe_pc_stage_reg;
wire[31:0] mem_pc_stage, mem_pc_stage_reg;
wire[31:0] wb_pc_stage, wb_pc_stage_reg;

wire [31:0] Result_WB, Val_Rn, Val_Rm, id_instruction_stage;
wire writeBackEn, hazard, WB_EN, MEM_R_EN, MEM_W_EN, B_out, S_out, imm, Two_src;
wire [3:0] Dest_wb, SR, EXE_CMD, Dest, src1, src2;
wire [11:0] Shift_operand;
wire [23:0] Signed_imm_24;

wire WB_EN_reg_id_out, MEM_R_EN_reg_id_out, MEM_W_EN_reg_id_out, B_reg_out, S_reg_out;
wire [3:0] EXE_CMD_out;
wire [31:0] val_Rn_id_out, val_Rm_id_out;

wire imm_id_out;
wire [11:0] Shift_operand_out;
wire [23:0] Signed_imm_24_out;
wire [3:0] Dest_out;
wire [31:0] id_instruction_stage_reg;
wire [3:0] src_1_out, src_2_out;

wire carry_out;

wire [31:0] ALU_result, Br_Addr, Val_Rm_exe_out;
wire [3:0] status_exe, Dest_exe_out;
wire WB_EN_exe_out, MEM_W_EN_exe_out, MEM_R_EN_exe_out, WB_EN_exe_reg_out, MEM_W_EN_exe_reg_out, MEM_R_EN_exe_reg_out;
wire WB_EN_mem_out, MEM_R_EN_mem_out, MEM_W_EN_mem_out;
wire [31:0] ALU_result_exe_reg_out, Val_Rm_exe_reg_out;
wire [31:0] ALU_result_mem_out;
wire [3:0] Dest_mem_out, Dest_exe_reg_out;
wire [31:0] Data_mem_out;

wire WB_EN_mem_reg_out, MEM_R_EN_mem_reg_out;
wire [31:0] ALU_result_mem_reg_out, Data_mem_reg_out;
wire [3:0] Dest_mem_reg_out;

wire [3:0] Dest_wb_out;
wire WB_EN_wb_out;
wire [31:0] outp;

assign freeze = 1'b0;
assign flush = 1'b0;


IF_Stage if_stage(clk, 
		  rst, 
		  freeze, 
		  branch_taken, 
	  	  branch_addr, 
		  if_pc_stage, 
		  if_instruction_stage);

IF_Stage_Reg if_stage_reg(clk,
			  rst, 
			  freeze, 
			  flush, 
			  if_pc_stage, 
			  if_instruction_stage, 
			  if_pc_stage_reg, 
			  if_instruction_stage_reg);

ID_Stage id_stage(clk,
		  rst, 
	          if_instruction_stage_reg,
 	          outp,
		  WB_EN_wb_out,
		  Dest_wb_out,
		  hazard,
		  SR,
		  if_pc_stage_reg,
 		  WB_EN,
		  MEM_R_EN,
		  MEM_W_EN, 
		  B_out, 
		  S_out, 
		  EXE_CMD,
 		  Val_Rn,
		  Val_Rm,
		  id_instruction_stage,
		  imm,
		  Shift_operand,
 		  Signed_imm_24,
		  Dest, 
		  src1, 
		  src2, 
		  Two_src,
		  id_pc_stage);


ID_Stage_Reg id_stage_reg(clk,
			  rst,
			  flush,
			  WB_EN,
		  	  MEM_R_EN,
		  	  MEM_W_EN,
			  B_out, 
		  	  S_out,
			  EXE_CMD,		  			 
 			  id_pc_stage,
			  Val_Rn,
		  	  Val_Rm,
			  imm,
			  Shift_operand,
 		  	  Signed_imm_24,
		  	  Dest,
		  	  id_instruction_stage,
			  src1, 
		  	  src2,
			  SR[2],
		          WB_EN_reg_id_out,
			  MEM_R_EN_reg_id_out,
			  MEM_W_EN_reg_id_out,
			  B_reg_out,
			  S_reg_out,
			  EXE_CMD_out,		  	  
 			  id_pc_stage_reg,
			  val_Rn_id_out,
			  val_Rm_id_out,
			  imm_id_out,
			  Shift_operand_out,
		 	  Signed_imm_24_out,
			  Dest_out,
			  id_instruction_stage_reg,
			  src_1_out,
			  src_2_out,
			  carry_out		  
);

EXE_Stage exe_stage(clk, 
		    rst, 
		    EXE_CMD_out,
		    outp,
		    MEM_R_EN_reg_out,
	   	    MEM_W_EN_reg_out,
		    id_pc_stage_reg,
		    val_Rn_id_out,
		    val_Rm_id_out,
		    imm_id_out,
		    Shift_operand_out,
		    Signed_imm_24_out,
		    carry_out,
		    Dest_out,
		    ALU_result,
		    Br_Addr,
		    Val_Rm_exe_out,
		    status_exe,
		    Dest_exe_out,
		    WB_EN_exe_out, 
		    MEM_W_EN_exe_out, 
		    MEM_R_EN_exe_out,
	 	    exe_pc_stage
		    );

EXE_Stage_Reg exe_stage_reg(clk,
			    rst, 
			    freeze, 
			    flush, 
			    WB_EN_exe_out, 
		    	    MEM_W_EN_exe_out, 
		    	    MEM_R_EN_exe_out,    
		 	    exe_pc_stage, 
			    ALU_result,
		 	    Val_Rm_exe_out,
			    Dest_exe_out, 
		            exe_pc_stage_reg,
			    WB_EN_exe_reg_out, 
		    	    MEM_R_EN_exe_reg_out, 
		    	    MEM_W_EN_exe_reg_out,
			    ALU_result_exe_reg_out,
			    Val_Rm_exe_reg_out,
			    Dest_exe_reg_out			    
);

MEM_Stage mem_stage(clk, 
		    rst, 
	 	    exe_pc_stage_reg,
		    WB_EN_exe_reg_out, 
		    MEM_R_EN_exe_reg_out, 
		    MEM_W_EN_exe_reg_out,
		    ALU_result_exe_reg_out,
		    Val_Rm_exe_reg_out,
		    Dest_exe_reg_out,   
		    mem_pc_stage,
		    WB_EN_mem_out, 
		    MEM_R_EN_mem_out, 
		    MEM_W_EN_mem_out,
		    ALU_result_mem_out,
		    Dest_mem_out,
		    Data_mem_out
);

MEM_Stage_Reg mem_stage_reg(clk, 
			    rst,
			    flush ,
			    freeze, 
			    WB_EN_mem_out, 
		   	    MEM_R_EN_mem_out,
			    mem_pc_stage,
			    ALU_result_mem_out, 
		   	    Data_mem_out,
		 	    Dest_mem_out,
			    mem_pc_stage_reg,
			    WB_EN_mem_reg_out, 
		   	    MEM_R_EN_mem_reg_out,
			    ALU_result_mem_reg_out, 
		   	    Data_mem_reg_out,
			    Dest_mem_reg_out
);

WB_Stage wb_stage(clk, 
		  rst, 
		  mem_pc_stage_reg,
		  Dest_mem_reg_out,
		  ALU_result_mem_reg_out,
		  Data_mem_reg_out,
		  MEM_R_EN_mem_reg_out,
		  WB_EN_mem_reg_out, 	 	    
		  wb_pc_stage,
		  Dest_wb_out,
		  WB_EN_wb_out,
	 	  outp
);

Status_Register status_regist(clk,
			      rst,
			      S_out,
			      status_exe,
			      SR
);


endmodule
module Mem_testBench();

reg flush = 0;
reg freeze = 0;
reg clk = 0;
reg rst = 0;
reg WB_en = 0; reg Mem_R_en = 0; reg Mem_W_en = 1;
reg [31:0] alu_res = 32'd4;
reg [31:0] val_rm = 32'b0;
reg [31:0] pc_in = 32'd1024;
reg [3:0] dest = 4'b0000;
wire WB_en_out, Mem_R_en_out, Mem_reg_WB_en, Mem_reg_R_en;
wire [31:0] Mem_res_out, data_mem_out, pc, Mem_reg_pc, Mem_reg_alu_res, Mem_reg_data_mem;
wire [3:0] dest_out, Mem_reg_dest;

initial begin
    #19 rst = 1;
    #13 rst = 0;
end

always begin
clk = #50 !clk;
end

MEM_Stage mem_stage(clk, rst, pc_in, WB_en, Mem_R_en, Mem_W_en, 
            alu_res, val_rm, dest, pc, WB_en_out, Mem_R_en_out,
            Mem_res_out, dest_out, data_mem_out);
            
MEM_Stage_Reg mem_stage_reg(clk, rst, flush, freeze, WB_en_out,
                Mem_R_en_out, pc, alu_res, dest_out, data_mem_out,
                Mem_reg_pc, Mem_reg_WB_en, Mem_reg_R_en,
                Mem_reg_alu_res, Mem_reg_dest, Mem_reg_data_mem);

endmodule


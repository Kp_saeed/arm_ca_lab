module EXE_testBench();

reg flush = 0;
reg freeze = 0;
reg clk = 0;
reg rst = 0;
reg WB_en = 0; reg mem_R_en = 0; reg mem_W_en = 1;
reg imm = 0; reg C = 0;
reg [31:0] pc_in = 32'd1024;
reg [31:0] val_rn = 32'd0;
reg [31:0] val_rm = 32'd1;
reg [3:0] EXE_cmd = 4'b0100;
reg [3:0] SR = 4'b0010;
reg [3:0] dest = 4'b0001;
reg [11:0] shift_operand = 12'd1;
reg [23:0] Signed_imm = 24'd2;

wire WB_en_out, Mem_R_en_out, Mem_W_en_out,
EXE_reg_WB_en, EXE_reg_mem_R_en, EXE_reg_mem_W_en;
wire [31:0] alu_res, Br_addr, pc, val_rm_out,
EXE_reg_pc, EXE_reg_alu_res, EXE_reg_ST_val;
wire [3:0] dest_out, status, EXE_reg_dest;

initial begin
    #19 rst = 1;
    #13 rst = 0;
end

always begin
clk = #50 !clk;
end

EXE_Stage mem_stage(clk, rst, EXE_cmd, WB_en, mem_R_en, mem_W_en,
              pc_in, val_rn, val_rm, imm, shift_operand, Signed_imm,
              SR, C, dest, alu_res, Br_addr, val_rm_out,status, dest_out,
              WB_en_out, Mem_W_en_out, Mem_R_en_out, pc);
            
EXE_Stage_Reg mem_stage_reg(clk, rst, freeze, flush, WB_en_out, Mem_R_en_out,
                  Mem_W_en_out, pc, alu_res, val_rm_out,dest_out, EXE_reg_pc,
                  EXE_reg_WB_en, EXE_reg_mem_R_en, EXE_reg_mem_W_en,
                  EXE_reg_alu_res, EXE_reg_ST_val, EXE_reg_dest);

endmodule

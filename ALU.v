module ALU(
	input [31:0] in1, in2,
	input [3:0] EXE_Command,
	input C,
	output [31:0] result,
	output [3:0] status
);

reg V1, C1;
wire N1, Z1;
reg [32:0] temp;

always @(*) begin
	V1 = 1'b0;
        C1 = 1'b0;
        case (EXE_Command)
        	4'b0001: begin
        		temp = in2;
            	end
            	4'b1001: begin
                	temp = ~in2;
            	end
            	4'b0010: begin
                	temp = in1 + in2;
                	C1 = temp[32];
                	V1 = (in1[31] ~^ in2[31]) & (temp[31] ^ in1[31]);
            	end
            	4'b0011: begin
                	temp = in1 + in2 + C;
                	C1 = temp[32];
                	V1 = (in1[31] ~^ in2[31]) & (temp[31] ^ in1[31]);
            	end
            	4'b0100: begin
                	temp = in1 - in2;
                	C1 = temp[32];
                	V1 = (in1[31] ^ in2[31]) & (temp[31] ^ in1[31]);

            	end
            	4'b0101: begin
    	           	temp = in1 - in2 - 2'b01;
        	        C1 = temp[32];
    	            	V1 = (in1[31] ^ in2[31]) & (temp[31] ^ in1[31]);
        	end
    	        4'b0110: begin
        	        temp = in1 & in2;
   	        end
        	4'b0111: begin
    	            	temp = in1 | in2;
	     	end
    	        4'b1000: begin
        	        temp = in1 ^ in2;
   	     	end
        	4'b0100: begin
    	            	temp = in1 - in2;
        	    	C1 = temp[32];
    	            	V1 = (in1[31] ^ in2[31]) & (temp[31] ^ in1[31]);
            	end
            	4'b0110: begin
                    	temp = in1 & in2;
            	end
            	4'b0010: begin
                    	temp = in1 + in2;
            	end
            	4'b0010: begin
                    	temp = in1 + in2;
            end
            default:
               	 	temp = 3'bx;

        endcase
    end

assign result = temp[31:0];
assign status = {Z1, C1, N1, V1};

assign N1 = result[31];
assign Z1 = ~(|result);

endmodule
	

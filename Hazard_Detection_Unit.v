module hazard_Detection_Unit(
	input [4:0] src1,
	input [4:0] src2,
	input [4:0] Exe_Dest,
	input Exe_WB_EN,
	input [4:0] Mem_Dest,
	input Mem_WB_EN,
	input With_Two_Source,
	output hazard_Detected
);

wire hazard_with_fwd, hazard_without_fwd;


assign hazard_Detected = ( Exe_WB_EN == 1'b1 & src1 == Exe_Dest ) | 
			 ( Mem_WB_EN == 1'b1 & src1 == Mem_Dest ) | 
                         ( Exe_WB_EN == 1'b1 & src2 == Exe_Dest & With_Two_Source == 1'b1) | 
                         ( Mem_WB_EN == 1'b1 & src2 == Mem_Dest & With_Two_Source == 1'b1) ? 1'b1 : 1'b0;

endmodule
	

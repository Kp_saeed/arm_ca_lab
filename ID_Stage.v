module ID_Stage (
	input clk,
	input rst,
	input [31:0] Instruction,
	input [31:0] Result_WB,
	input writeBackEn,
	input [3:0] Dest_wb,
	input hazard,
	input [3:0] SR,
	input[31:0] PC_in,

	output WB_EN, MEM_R_EN, MEM_W_EN, B_out, S_out,
	output [3:0] EXE_CMD,
	output [31:0] Val_Rn, Val_Rm,
	output [31:0] Instruction_out,
	output imm,
	output [11:0] Shift_operand,
	output [23:0] Signed_imm_24,
	output [3:0] Dest,
	output [3:0] src1, src2,
	output Two_src,
	output[31:0] PC
);

wire [3:0] EXE_Command;
wire mem_read, mem_write, WB_Enable, B, Status_Update;
wire check;
wire selector;
wire Rm;
wire [8:0] mux_in;

assign Dest = Instruction[15:12];
assign src1 = Instruction[19:16];
assign Rm = Instruction[3:0];
assign selector = (~check) | hazard;

assign src2 = MEM_W_EN ? Dest : Rm;

RegisterFile register_file(clk, rst, src1, src2, Dest_wb, Result_WB, writeBackEn, Val_Rn, Val_Rm);

assign mux_in =  {Status_Update, B, EXE_Command, mem_write, mem_read, WB_Enable};
assign {S_out, B_out, EXE_CMD, MEM_W_EN, MEM_R_EN, WB_EN} = selector ?  9'b0 : mux_in;

ControlUnit control_unit(Instruction[27:26], Instruction[24:21], Instruction[20], EXE_Command, mem_read, mem_write, WB_Enable, B, Status_Update);

Condition_Check condition_check(Instruction[31:28], SR, check);

assign PC = PC_in;
assign Instruction_out = Instruction;
assign Shift_operand = Instruction[11:0];
assign Signed_imm_24 = Instruction[23:0];
assign imm = Instruction[25];

endmodule
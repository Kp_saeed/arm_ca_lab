module Memory(
    input clk, rst, Mem_W_en, Mem_R_en, 
    input [31:0] addr, 
    input [31:0] data,
    output [31:0] res);

    reg[7:0] mem [0:255];

    wire[31:0] start_addr_0;
    assign start_addr_0 = {addr[31:2], 2'b00} - 32'd1024;

    assign res = (Mem_R_en == 1'b1 ? {mem[start_addr_0],
          mem[{start_addr_0[31:1], 1'b1}],
          mem[{start_addr_0[31:2], 2'b10}],
          mem[{start_addr_0[31:2], 2'b11}]} : 32'b0);

    always@(posedge clk) begin
        if(Mem_W_en == 1'b1) begin
            mem[{start_addr_0[31:2], 2'b11}] <= data[7:0];
            mem[{start_addr_0[31:2], 2'b10}] <= data[15:8];
            mem[{start_addr_0[31:1], 1'b1}] <= data[23:16];
            mem[start_addr_0] <= data[31:24];
        end
    end
endmodule



